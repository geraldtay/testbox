/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.oss.testbox.abstraction;

/**
 *
 * @author gerald
 */
public abstract class InstallerAbstraction {
    
    public InstallerAbstraction() {
        
    }
    
    public abstract void setOption();
    
    public abstract void optionDoFinal();
    
    
}
