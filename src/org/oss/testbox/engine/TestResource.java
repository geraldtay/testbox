/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.engine;

import java.util.ArrayList;
import org.oss.testbox.plugin.AbstractTestCase;
import org.oss.testbox.plugin.AbstractTestStep;

/**
 *
 * @author gerald
 */
public class TestResource {

    private final ArrayList<AbstractTestCase> testCases;
    private final ArrayList<AbstractTestStep> testSteps;

    public TestResource() {
        testCases = new ArrayList<AbstractTestCase>();
        testSteps = new ArrayList<AbstractTestStep>();
    }

    public void addTestCase(AbstractTestCase testCase) {
        testCases.add(testCase);
    }

    public void addTestStep(AbstractTestStep testStep) {
        testSteps.add(testStep);
    }

    public ArrayList<AbstractTestCase> getTestCaseList() {
        return testCases;
    }

    public ArrayList<AbstractTestStep> getTestStepList() {
        return testSteps;
    }

    public int testCasesSize() {
        return testCases.size();
    }

    public int testStepsSize() {
        return testSteps.size();
    }
}
