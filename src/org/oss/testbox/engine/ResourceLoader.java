/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.engine;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 *
 * @author gerald
 */
public class ResourceLoader {

    public Class[] getClassResource(URL loadLocation) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        ClassLoader loader = URLClassLoader.newInstance(
                new URL[]{loadLocation},
                getClass().getClassLoader()
        );
        /*
         Class<?> clazz = Class.forName("mypackage.MyClass", true, loader);
         Class<? extends Runnable> runClass = clazz.asSubclass(Runnable.class);
         Constructor<? extends Runnable> ctor = runClass.getConstructor();
         Runnable doRun = ctor.newInstance();
         doRun.run();
         */
        return null;
    }

}
