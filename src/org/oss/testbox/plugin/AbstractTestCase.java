/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.plugin;

/**
 *
 * @author gerald
 */
public abstract class AbstractTestCase extends AbstractTestAction {

    private String testCaseName;
    private String status;
    private String comment;

    public String comment() {
        return comment;
    }
    
    public String status() {
        return status;
    }

}
