/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.plugin.sample;

import org.oss.testbox.plugin.AbstractTestStep;

/**
 *
 * @author gerald
 */
public class TestStep2 extends AbstractTestStep {

    public TestStep2() {
        super();
        this.setComment("More comments");
        this.setStepName("2nd step");
        this.setStepNum(2);
    }
    
    
    @Override
    public void doStep() {
        System.out.println("This is my second test step ?!");
    }

}
