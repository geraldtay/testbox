/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.plugin.sample;

import org.oss.testbox.plugin.AbstractTestStep;

/**
 *
 * @author gerald
 */
public class TestStep extends AbstractTestStep {

    public TestStep() {
        super();
        this.setComment("Test step");
        this.setComment("My very first test step...");
        this.setStepNum(1);
    }

    @Override
    public void doStep() {
        System.out.println("This is my very first step....");
    }

}
