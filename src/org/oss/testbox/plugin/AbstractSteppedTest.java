/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.oss.testbox.plugin;

/**
 *
 * @author gerald
 */
public interface AbstractSteppedTest {
    
    public abstract void defineStep(int stepNum, AbstractTestStep step);
    
    public abstract void doStep(int stepNum);
    
    public abstract void size();
    
    public abstract void getStep(int stepNum);
    
    public abstract void listSteps();
    
}
