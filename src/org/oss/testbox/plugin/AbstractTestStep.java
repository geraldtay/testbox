/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.plugin;

/**
 *
 * @author gerald
 */
public abstract class AbstractTestStep {

    private int stepNum= 0;
    private String stepName = "";
    private String comment = "";
    
    public abstract void doStep();

    /**
     * @return the stepNum
     */
    public int getStepNum() {
        return stepNum;
    }

    /**
     * @param stepNum the stepNum to set
     */
    public void setStepNum(int stepNum) {
        this.stepNum = stepNum;
    }

    /**
     * @return the stepName
     */
    public String getStepName() {
        return stepName;
    }

    /**
     * @param stepName the stepName to set
     */
    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

}
