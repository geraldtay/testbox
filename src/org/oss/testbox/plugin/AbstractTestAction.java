/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oss.testbox.plugin;

/**
 *
 * @author gerald
 */
public abstract class AbstractTestAction {

    public abstract void prepTest();

    public abstract void loadTest();

    public abstract void execTest();

    public abstract void postTest();

}
